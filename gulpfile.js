// eslint-disable-next-line no-mixed-requires
const gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('autoprefixer'),
  browsersync = require('browser-sync').create(),
  sourcemaps = require('gulp-sourcemaps'),
  rename = require('gulp-rename'),
  ts = require('gulp-typescript'),
  tsProject = ts.createProject('tsconfig.json'),
  concat = require('gulp-concat'),
  clean = require('gulp-clean'),
  babel = require('gulp-babel'),
  postcss = require('gulp-postcss'),
  debug = require('gulp-debug'),
  changed = require('gulp-changed'),
  eslint = require('gulp-eslint'),
  cssnano = require('cssnano'),
  plumber = require('gulp-plumber'),
  svgSprite = require('gulp-svg-sprite'),
  imagemin = require('gulp-imagemin'),
  iconfont = require('gulp-iconfont'),
  iconfontCss = require('gulp-iconfont-css'),
  spritesmith = require('gulp.spritesmith'),
  uglify = require('gulp-uglify-es').default;

const { src, dest, watch, series, parallel } = require('gulp');

const files = {
  scssPath: './app/sass/**/*.scss',
  vendorCssPath: './app/sass/vendor/*.*',
  vendorDeployCssPath: './app/sass/vendor/*.css',
  jsPath: './app/js/**/*.js',
  tsPath: './app/ts/**/*.ts',
  htmlPath: './app/html/**/*.*',
  imgPath: './app/images/**/*.*',
  svgPath: './app/svg/**/*.svg',
  includesPath: './app/includes/*.*',
  iconsPath: './app/icons/*.*',
  iconsFontPath: './app/svgfonts/*.svg',
  fontPath: './app/fonts/**/*.{eot,svg,ttf,woff,woff2}',
};

function linter() {
  const startValue = 0;
  return (
    src(files.jsPath)
      .pipe(eslint({ fix: true }))
      .pipe(eslint.format())
      // .pipe(eslint.failAfterError())
      .pipe(
        eslint.results((results) => {
          let errorCount = results.errorCount,
            warnCount = results.warningCount;

          if (errorCount > startValue || warnCount > startValue) {
            console.log(
              '\x1b[31m',
              '---------------------------------------------------',
              '\x1b[0m'
            );
            // console.log('\x1b[32m', ' ' ,'\x1b[0m');
            console.log('\x1b[31m', `Total ESLint Error Count: ${errorCount}`);
            console.log('\x1b[31m', `Total ESLint Warnings Count: ${warnCount}`);
            // console.log('\x1b[32m', ' ' ,'\x1b[0m');
          }
          if (errorCount === startValue && warnCount === startValue) {
            console.log(
              '\x1b[32m',
              '---------------------------------------------------',
              '\x1b[0m'
            );
            // console.log('\x1b[32m', ' ' ,'\x1b[0m');
            console.log('\x1b[32m', 'All linter rules successfully passed...', '\x1b[0m');
            console.log('\x1b[32m', 'No errors or warnings.found.', '\x1b[0m');
            // console.log('\x1b[32m', ' ' ,'\x1b[0m');
          }
        })
      )
      .on('end', () => {
        console.log(
          '\x1b[33m',
          '---------------------------------------------------',
          '\x1b[0m'
        );
        // console.log('\x1b[32m', ' ' ,'\x1b[0m');
        console.log('\x1b[33m', 'Linter tasks have been completed...', '\x1b[0m');
        // console.log('\x1b[32m', ' ' ,'\x1b[0m');
        console.log(
          '\x1b[33m',
          '---------------------------------------------------',
          '\x1b[0m'
        );
      })
  );
}

function pngSpritesTask() {
  let spriteData = gulp.src(files.iconsPath).pipe(
    spritesmith({
      imgName: 'sprites.png',
      cssName: 'sprites.scss',
      imgPath: '../img/sprites.png',
      algorithm: 'binary-tree',
      cssVarMap: (sprite) => {
        sprite.name = `sprite-${sprite.name}`;
      },
      padding: 20,
    })
  );

  spriteData.img.pipe(gulp.dest('./dist/img'));
  spriteData.css.pipe(gulp.dest('./app/sass/source/'));

  return spriteData;
}

function pngSpritesDeployTask() {
  let spriteData = gulp.src(files.iconsPath).pipe(
    spritesmith({
      imgName: 'sprites.png',
      imgPath: '../img/sprites.png',
      cssName: 'sprites.css',
      algorithm: 'binary-tree',
      padding: 20,
    })
  );

  spriteData.img.pipe(gulp.dest('./dist/img'));

  return spriteData;
}

function vendorCssTask() {
  const DESTINATION = 'dist/css';

  return src(files.vendorCssPath)
    .pipe(sourcemaps.write('')) // write sourcemaps file in current directory
    .pipe(
      debug({
        title: 'Vendor CSS files',
      })
    )
    .pipe(dest(DESTINATION)); // put final CSS in dist folder
}

function vendorDeployCssTask() {
  const DESTINATION = 'dist/css';

  return src(files.vendorDeployCssPath)
    .pipe(sourcemaps.write('')) // write sourcemaps file in current directory
    .pipe(dest(DESTINATION)); // put final CSS in dist folder
}

function scssTask() {
  const DESTINATION = 'dist/css';

  return src(files.scssPath)
    .pipe(changed(DESTINATION))
    .pipe(sourcemaps.init()) // initialize sourcemaps first
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('styles.css')) // concatenate all sass files
    .pipe(
      postcss([
        autoprefixer({
          overrideBrowserslist: [ 'last 2 versions' ],
          cascade: false,
        }),
        cssnano,
      ])
    ) // PostCSS plugins
    .pipe(sourcemaps.write('')) // write sourcemaps file in current directory
    .pipe(plumber.stop())
    .pipe(
      debug({
        title: 'CSS files',
      })
    )
    .pipe(dest(DESTINATION)) // put final CSS in dist folder
    .pipe(browsersync.stream());
}

function scssTaskDeploy() {
  return src(files.scssPath)
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('styles.css')) // concatenate all sass files
    .pipe(
      postcss([
        autoprefixer({
          overrideBrowserslist: [ 'last 2 versions' ],
          cascade: false,
        }),
        cssnano,
      ])
    ) // PostCSS plugins
    .pipe(dest('dist/css')); // put final CSS in dist folder
}

function jsTaskDeploy() {
  return src(files.jsPath)
    .pipe(concat('main.js'))
    .pipe(
      uglify({
        mangle: false,
        ecma: 6,
      })
    )
    .pipe(gulp.dest('dist/js'));
}

function tsTaskDeploy() {
  return src(files.tsPath)
    .pipe(tsProject())
    .pipe(concat('main.js'))
    .pipe(
      uglify({
        mangle: false,
        ecma: 6,
      })
    )
    .pipe(gulp.dest('dist/js'));
}

function jsTask() {
  const DESTINATION = 'dist/js';

  return src(files.jsPath)
    .pipe(changed(DESTINATION))
    .pipe(sourcemaps.init())
    .pipe(concat('main.js'))
    .pipe(
      uglify({
        mangle: false,
        ecma: 6,
      })
    )
    .pipe(sourcemaps.write())
    .pipe(
      debug({
        title: 'JS files',
      })
    )
    .pipe(gulp.dest(DESTINATION))
    .pipe(browsersync.stream());
}

function tsTask() {
  const DESTINATION = 'dist/js';

  return gulp
    .src(files.tsPath)
    .pipe(tsProject())
    .pipe(sourcemaps.init())
    .pipe(
      uglify({
        mangle: false,
        ecma: 6,
      })
    )
    .pipe(sourcemaps.write())
    .pipe(
      debug({
        title: 'TS files',
      })
    )
    .pipe(gulp.dest(DESTINATION))
    .pipe(browsersync.stream());
}

// function tsTask(){
//     const DESTINATION = 'dist/js'
//
//     return gulp.src(files.tsPath)
//         .pipe(tsProject())
//         .pipe(concat('main.js'))
//         .pipe(sourcemaps.init())
//         .pipe(uglify({
//             mangle: false,
//             ecma: 6
//         }))
//         .pipe(sourcemaps.write())
//         .pipe(debug({
//             "title": "TS files"
//         }))
//         .pipe(gulp.dest(DESTINATION))
//         .pipe(browsersync.stream());
// }

// function jsBabel() {
//   const DESTINATION = 'dist/js';
//
//   return src(files.jsPath)
//     .pipe(changed(DESTINATION))
//     .pipe(sourcemaps.init())
//     .pipe(concat('main.js'))
//     .pipe(
//       babel({
//         presets: ['@babel/env'],
//       })
//     )
//     .pipe(
//       rename({
//         suffix: '.es5',
//       })
//     )
//     .pipe(uglify())
//     .pipe(sourcemaps.write())
//     .pipe(
//       debug({
//         title: 'Babel',
//       })
//     )
//     .pipe(gulp.dest(DESTINATION))
//     .pipe(browsersync.stream());
// }

function jsBabelDeploy() {
  return src(files.jsPath)
    .pipe(concat('main.js'))
    .pipe(
      babel({
        presets: [ '@babel/env' ],
      })
    )
    .pipe(
      rename({
        suffix: '.es5',
      })
    )
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
}

function htmlTask() {
  const DESTINATION = './dist/';

  return src(files.htmlPath)
    .pipe(changed(DESTINATION))
    .pipe(
      debug({
        title: 'Html files',
      })
    )
    .pipe(gulp.dest(DESTINATION))
    .pipe(browsersync.stream());
}

function includesTask() {
  const DESTINATION = './dist/includes/';

  return src(files.includesPath)
    .pipe(changed(DESTINATION))
    .pipe(gulp.dest(DESTINATION))
    .pipe(browsersync.stream());
}

function imgTask() {
  const DESTINATION = './dist/img';

  return src(files.imgPath)
    .pipe(changed(DESTINATION))
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ quality: 75, progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [
            { removeViewBox: false },
            { removeUnusedNS: false },
            { removeUselessStrokeAndFill: false },
            { cleanupIDs: false },
            { removeComments: true },
            { removeEmptyAttrs: true },
            { removeEmptyText: true },
            { collapseGroups: true },
          ],
        }),
      ])
    )
    .pipe(
      debug({
        title: 'Images files',
      })
    )
    .pipe(dest(DESTINATION))
    .pipe(browsersync.stream());
}

function svgSpriteTask() {
  const DESTINATION = 'dist/img';

  return src(files.svgPath)
    .pipe(changed(DESTINATION))
    .pipe(
      svgSprite({
        mode: {
          stack: {
            sprite: '../sprites.svg',
          },
        },
      })
    )
    .pipe(
      debug({
        title: 'SVG sprite',
      })
    )
    .pipe(gulp.dest(DESTINATION));
}

function iconFont() {
  const DESTINATION = 'dist/css/fonts/';
  const fontName = 'iconfont';

  return src(files.iconsFontPath)
    .pipe(
      iconfontCss({
        fontName: fontName,
        targetPath: '../../../app/sass/source/iconFonts.scss',
        fontPath: '../css/fonts/',
      })
    )
    .pipe(
      iconfont({
        fontName: fontName,
        // Remove woff2 if you get an ext error on compile
        formats: [ 'svg', 'ttf', 'eot', 'woff', 'woff2' ],
        normalize: true,
        fontHeight: 1001,
      })
    )
    .pipe(
      debug({
        title: 'Svg ico font file: ',
      })
    )
    .pipe(gulp.dest(DESTINATION));
}

function fontsCopy() {
  const DESTINATION = 'dist/css/fonts/';

  return src(files.fontPath)
    .pipe(
      debug({
        title: 'Font file: ',
      })
    )
    .pipe(gulp.dest(DESTINATION));
}

function clear() {
  return src('./dist/*', {
    read: false,
  })
    .pipe(
      debug({
        title: 'Clean task files: ',
      })
    )
    .pipe(clean());
}

function browserSync() {
  browsersync.init({
    server: {
      baseDir: 'dist',
    },
    port: 3000,
  });
}

function watchTask() {
  watch(files.scssPath, parallel(scssTask));

  watch(files.jsPath, parallel(jsTask));

  watch(files.tsPath, parallel(tsTask));

  watch(files.htmlPath, parallel(htmlTask));

  watch(files.includesPath, parallel(includesTask));
}

exports.linter = parallel(linter);
exports.clean = series(clear);
exports.watch = parallel(watchTask, browserSync);
exports.watchall = series(
  clear,
  [ iconFont ],
  parallel(
    scssTask,
    jsTask,
    tsTask,
    htmlTask,
    includesTask,
    vendorCssTask,
    imgTask,
    svgSpriteTask,
    pngSpritesTask,
    fontsCopy
  ),
  parallel(watchTask, browserSync)
);
exports.images = parallel(imgTask, svgSpriteTask, pngSpritesTask, iconFont, fontsCopy);
exports.deploy = series(
  clear,
  [ iconFont ],
  parallel(
    scssTaskDeploy,
    jsTaskDeploy,
    tsTaskDeploy,
    jsBabelDeploy,
    htmlTask,
    includesTask,
    vendorDeployCssTask,
    imgTask,
    svgSpriteTask,
    pngSpritesDeployTask,
    fontsCopy
  )
);
exports.default = series(
  clear,
  [ iconFont ],
  parallel(
    scssTask,
    jsTask,
    tsTask,
    htmlTask,
    includesTask,
    vendorCssTask,
    imgTask,
    svgSpriteTask,
    pngSpritesTask,
    fontsCopy
  )
);
